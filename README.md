# Magento 1.9.x Extension (Payl8r_PaymentGateway) #

### What is this repository for? ###

Here you can find an up to date production version of the Magento 1.9.x Payl8r Payment Method Extension.

The extension adds a new payment method to Magento for customers to pay on finance using Payl8r.

### How do I get set up? ###

The extension can be installed either with the Magento Package Manager (Payl8r_PaymentGateway-1.0.0.1.tgz) or by merging the app and skin folders inside Manual with your Magento install.

It is highly recommended to properly test in a none production environment prior to release.